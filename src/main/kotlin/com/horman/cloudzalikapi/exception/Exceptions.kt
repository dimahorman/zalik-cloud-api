package com.horman.cloudzalikapi.exception

open class BusinessException(
    open val code: String,
    open val description: String
) : Throwable("$code: $description", null, true, false)

class RecordNotFound : BusinessException("RECORD_NOT_FOUND", "No record found matches provided options")
