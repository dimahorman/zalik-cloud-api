package com.horman.cloudzalikapi.config

import com.horman.cloudzalikapi.util.Secrets
import com.mongodb.reactivestreams.client.MongoClient
import com.mongodb.reactivestreams.client.MongoClients
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories

@Configuration
@EnableReactiveMongoRepositories
class MongoDatabaseConfig {
    @Bean("mongo_client")
    fun getMongoClient(): MongoClient {
        val host =
            Secrets.get("MONGODB_HOST") ?: throw RuntimeException("Please define MONGODB_HOST environment variable")
        val port =
            Secrets.get("MONGODB_PORT") ?: throw RuntimeException("Please define MONGODB_PORT environment variable")
        return MongoClients.create("mongodb://$host:$port")
    }
}

@Configuration
class ReactiveMongoConfig(@Qualifier("mongo_client") val mongoClient: MongoClient) {
    @Bean("mongo_template")
    fun getReactiveMongoTemplate(): ReactiveMongoTemplate {
        val db = Secrets.get("MONGODB_DATABASE")
            ?: throw RuntimeException("Please define MONGODB_DATABASE environment variable")
        return ReactiveMongoTemplate(mongoClient, db)
    }
}
