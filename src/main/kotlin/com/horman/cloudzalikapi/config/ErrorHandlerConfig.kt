package com.horman.cloudzalikapi.config

import com.horman.cloudzalikapi.exception.BusinessException
import org.springframework.boot.autoconfigure.web.ResourceProperties
import org.springframework.boot.autoconfigure.web.reactive.error.AbstractErrorWebExceptionHandler
import org.springframework.boot.web.reactive.error.DefaultErrorAttributes
import org.springframework.boot.web.reactive.error.ErrorAttributes
import org.springframework.context.ApplicationContext
import org.springframework.core.annotation.Order
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.codec.ServerCodecConfigurer
import org.springframework.stereotype.Component
import org.springframework.validation.BindingResult
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.server.*
import org.springframework.web.server.ResponseStatusException

@Component
class AppErrorAttributes(
) : DefaultErrorAttributes() {
    override fun getErrorAttributes(request: ServerRequest, includeStackTrace: Boolean): Map<String, Any?> {
        val attrs = mutableMapOf<String, Any?>()

        when (val error = getError(request)) {
            is BusinessException -> run {
                attrs["code"] = error.code
                attrs["description"] = error.description
                attrs["status"] = HttpStatus.UNPROCESSABLE_ENTITY.value()
            }
            is BindingResult -> run {
                val errors = if (error.hasErrors()) ". Errors: ${error.allErrors.joinToString()}" else ""
                attrs["code"] = HttpStatus.BAD_REQUEST.name
                attrs["description"] = error.message + errors
                attrs["status"] = HttpStatus.BAD_REQUEST.value()
            }

            is ResponseStatusException -> run {
                attrs["code"] = error.status.name
                attrs["description"] = error.message
                attrs["status"] = error.status.value()
            }
            else -> run {
                attrs["code"] = HttpStatus.INTERNAL_SERVER_ERROR.name
                attrs["description"] = error.message
                attrs["status"] = HttpStatus.INTERNAL_SERVER_ERROR.value()
            }
        }

        return attrs
    }
}

@Component
@Order(-2)
class GlobalErrorWebExceptionHandler(
    errorAttributes: ErrorAttributes,
    resourceProperties: ResourceProperties,
    applicationContext: ApplicationContext,
    serverCodecConfigurer: ServerCodecConfigurer
) : AbstractErrorWebExceptionHandler(errorAttributes, resourceProperties, applicationContext) {
    init {
        this.setMessageWriters(serverCodecConfigurer.writers)
        this.setMessageReaders(serverCodecConfigurer.readers)
    }

    private val handlerFunction = HandlerFunction { req ->
        val resp = getErrorAttributes(req, false)
        val httpStatus = resp.remove("status") as Int

        ServerResponse.status(httpStatus)
            .contentType(MediaType.APPLICATION_JSON)
            .body(BodyInserters.fromValue<Map<String, Any>>(resp))
    }

    override fun getRoutingFunction(errorAttributes: ErrorAttributes): RouterFunction<ServerResponse> {
        return RouterFunctions.route(RequestPredicates.all(), handlerFunction)
    }
}
