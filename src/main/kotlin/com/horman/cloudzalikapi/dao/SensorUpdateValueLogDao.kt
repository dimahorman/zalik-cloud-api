package com.horman.cloudzalikapi.dao

import com.horman.cloudzalikapi.dto.SensorUpdateValueLog
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.reactive.awaitFirst
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.query.BasicQuery
import org.springframework.stereotype.Repository
import reactor.core.publisher.Mono

@Repository
class SensorUpdateValueLogDao(@Qualifier("mongo_template") private val template: ReactiveMongoTemplate) {
    suspend fun createLog(sensorUpdateValueLog: SensorUpdateValueLog): SensorUpdateValueLog {
        return template.save(Mono.just(sensorUpdateValueLog)).awaitFirst()
    }

    suspend fun findBySensorId(sensorId: Int): List<SensorUpdateValueLog> {
        val query = BasicQuery("{ sensorId: $sensorId }")
        return template.find(query, SensorUpdateValueLog::class.java).asFlow().toList()
    }
}