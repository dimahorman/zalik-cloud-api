package com.horman.cloudzalikapi.dao

import com.horman.cloudzalikapi.dto.Sensor
import com.mongodb.client.result.UpdateResult
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.reactive.awaitFirst
import kotlinx.coroutines.reactive.awaitFirstOrNull
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.core.query.Update
import org.springframework.stereotype.Repository
import reactor.core.publisher.Mono

@Repository
class SensorDao(@Qualifier("mongo_template") private val template: ReactiveMongoTemplate) {
    suspend fun findById(id: Int): Sensor? {
        return template.findById(id, Sensor::class.java).awaitFirstOrNull()
    }

    suspend fun findAll(): List<Sensor> {
        return template.findAll(Sensor::class.java).asFlow().toList()
    }

    suspend fun save(sensor: Sensor): Sensor {
        return template.save(Mono.just(sensor)).awaitFirst()
    }

    suspend fun updateValue(id: Int, value: Int): UpdateResult? {
        val query = Query(Criteria("id").`is`(id))
        val update = Update().set("value", value)
        return template.updateFirst(query, update, Sensor::class.java).awaitFirstOrNull()
    }
}