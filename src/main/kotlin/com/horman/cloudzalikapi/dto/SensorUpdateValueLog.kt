package com.horman.cloudzalikapi.dto

import org.springframework.data.annotation.Id

data class SensorUpdateValueLog(
    @Id
    val id: String,
    val sensorId: Int,
    val date: Long,
    val updatedValue: Int
)