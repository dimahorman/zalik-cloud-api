package com.horman.cloudzalikapi.dto

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document
data class Sensor(
    @Id
    val id: Int,
    val fishFarmId: String,
    val type: String,
    val value: Int,
)

