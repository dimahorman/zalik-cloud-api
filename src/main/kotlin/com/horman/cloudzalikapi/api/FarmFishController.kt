package com.horman.cloudzalikapi.api

import com.horman.cloudzalikapi.dto.Sensor
import com.horman.cloudzalikapi.dto.SensorUpdateValueLog
import com.horman.cloudzalikapi.service.SensorService
import com.mongodb.client.result.UpdateResult
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/sensors")
class FarmFishController(private val sensorService: SensorService) {
    @GetMapping("/{id}")
    suspend fun getSensorById(@PathVariable id: Int): Sensor {
        return sensorService.getSensorById(id)
    }

    @GetMapping
    suspend fun getSensors(): List<Sensor> {
        return sensorService.getSensors()
    }

    @PostMapping
    suspend fun addSensor(@RequestBody sensor: Sensor): Sensor {
        return sensorService.addOrUpdateSensor(sensor)
    }

    @PatchMapping("/{id}")
    suspend fun updateSensorValue(@PathVariable id: Int, @RequestParam value: Int): UpdateResult {
        return sensorService.updateValue(id, value)
    }

    @GetMapping("/{id}/logs")
    suspend fun getSensorLogsBySensorId(@PathVariable id: Int): List<SensorUpdateValueLog> {
        return sensorService.getLogsBySensorId(id)
    }
}