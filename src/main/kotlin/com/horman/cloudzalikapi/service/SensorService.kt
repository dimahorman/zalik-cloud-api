package com.horman.cloudzalikapi.service

import com.horman.cloudzalikapi.dao.SensorDao
import com.horman.cloudzalikapi.dao.SensorUpdateValueLogDao
import com.horman.cloudzalikapi.dto.Sensor
import com.horman.cloudzalikapi.dto.SensorUpdateValueLog
import com.horman.cloudzalikapi.exception.RecordNotFound
import com.horman.cloudzalikapi.util.Base64Generator
import com.mongodb.client.result.UpdateResult
import org.springframework.stereotype.Service

@Service
class SensorService(
    private val sensorDao: SensorDao,
    private val sensorUpdateValueLogDao: SensorUpdateValueLogDao
) {
    suspend fun getSensorById(id: Int): Sensor {
        return sensorDao.findById(id) ?: throw RecordNotFound()
    }

    suspend fun addOrUpdateSensor(sensor: Sensor): Sensor {
        return sensorDao.save(sensor)
    }

    suspend fun getSensors(): List<Sensor> {
        return sensorDao.findAll()
    }

    suspend fun updateValue(id: Int, value: Int): UpdateResult {
        val updateResult = sensorDao.updateValue(id, value) ?: throw RecordNotFound()
        if (updateResult.modifiedCount == 1L) {
            sensorUpdateValueLogDao.createLog(
                SensorUpdateValueLog(
                    Base64Generator.generate(),
                    id,
                    System.currentTimeMillis(),
                    value
                )
            )
        }
        return updateResult
    }

    suspend fun getLogsBySensorId(sensorId: Int): List<SensorUpdateValueLog> {
        return sensorUpdateValueLogDao.findBySensorId(sensorId)
    }
}