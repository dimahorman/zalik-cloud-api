package com.horman.cloudzalikapi.util

import java.util.*

object Base64Generator {
    fun generate(): String {
        val randomValue = (0..Int.MAX_VALUE).random()
        val hex = Integer.toHexString(randomValue)
        var str = ""
        Base64.getEncoder().encode(
            hex.map { it.toInt().toByte() }.toByteArray()
        )
            .forEach {
                str += it.toChar()
            }.toString()
        return str
    }
}
